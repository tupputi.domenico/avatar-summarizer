var express = require('express');
var router = express.Router();
var textToSpeech = require('../helpers/tts');

/* GET home page. */
router.post('/talk', function(req, res, next) {

  textToSpeech(req.body.text, req.body.messages)
  .then(result => {
    res.json(result);    
  })
  .catch(err => {
    res.json(err);
  });


});

module.exports = router;
