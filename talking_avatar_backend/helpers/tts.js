// azure-cognitiveservices-speech.js
require('dotenv').config()
const sdk = require('microsoft-cognitiveservices-speech-sdk');
const blendShapeNames = require('./blendshapeNames');
const _ = require('lodash');
const OpenAI = require("openai");
const axios = require("axios");
const {PythonShell} = require("python-shell");
let SSML = `<speak version="1.0" xmlns="http://www.w3.org/2001/10/synthesis" xmlns:mstts="http://www.w3.org/2001/mstts" xml:lang="en-US">
<voice name="en-US-JennyNeural">
  <mstts:viseme type="FacialExpression"/>
  __TEXT__
</voice>
</speak>`;

const key = 'df7e05b296794ee0b2e914cb4d145f86';
const region = 'eastus';

function makeSpeech(text) {
    return axios.post('https://content-texttospeech.googleapis.com/v1/text:synthesize?alt=json&key=AIzaSyC-Ky3b52eluten4oK46obgeviEl21soK0',
        {
            'input': {'text': text},
            'audioConfig': {"audioEncoding": "MP3"},
            'voice': {"languageCode": "it-CH"}
        });
}

const openai = new OpenAI({
    apiKey: "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",
});

async function pythonProcess(buffer, path) {
    let pyshell = new PythonShell('main.py', {mode: 'text'});

    // sends a message to the Python script via stdin
    await pyshell.send(path);
    await pyshell.send(buffer);

    await pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        console.log(message);
    });

    // end the input stream and allow the process to exit
    await pyshell.end(function (err, code, signal) {
        if (err) throw err;
        console.log('The exit code was: ' + code);
        console.log('The exit signal was: ' + signal);
        console.log('finished');
    });
}



/**
 * Node.js server code to convert text to speech
 * @returns stream
 * @param {*} key your resource key
 * @param {*} region your resource region
 * @param {*} text text to convert to audio/speech
 * @param {*} filename optional - best for long text - temp file for converted speech/audio
 */
const textToSpeech = async (text, messages) => {

    // convert callback function to promise
    return new Promise(async (resolve, reject) => {
        messages.push({role: "user", content: text + ' Tenendo in considerazione il testo iniziale?'})
        const chatCompletion = await openai.chat.completions.create({
            messages: messages,
            model: "gpt-3.5-turbo-16k-0613",
        });
        let chatResponse = chatCompletion.choices[0].message.content
        messages.push({role: "assistant", content: chatResponse})
        let randomString = Math.random().toString(36).slice(2, 7);
        let path = `./public/italy-${randomString}.mp3`;
        await makeSpeech(chatResponse)
            .then(async response => {

                await pythonProcess(response.data.audioContent, path);

            })
            .catch(err => {
                console.error(err);

            })

        let ssml = SSML.replace("__TEXT__", chatResponse);


        const speechConfig = sdk.SpeechConfig.fromSubscription(key, region);
        speechConfig.speechSynthesisOutputFormat = 5; // mp3

        let audioConfig = null;

        let filename = `./public/audio.mp3`;
        audioConfig = sdk.AudioConfig.fromAudioFileOutput(filename);

        let blendData = [];
        let timeStep = 1 / 60;
        let timeStamp = 0;

        const synthesizer = new sdk.SpeechSynthesizer(speechConfig, audioConfig);

        // Subscribes to viseme received event
        synthesizer.visemeReceived = function (s, e) {

            // `Animation` is an xml string for SVG or a json string for blend shapes
            var animation = JSON.parse(e.animation);

            _.each(animation.BlendShapes, blendArray => {

                let blend = {};
                _.each(blendShapeNames, (shapeName, i) => {
                    blend[shapeName] = blendArray[i];
                });

                blendData.push({
                    time: timeStamp,
                    blendshapes: blend
                });
                timeStamp += timeStep;
            });

        }


        synthesizer.speakSsmlAsync(
            ssml,
            result => {

                synthesizer.close();
                resolve({blendData, filename: `/italy-${randomString}.mp3`, textResponse: chatResponse, messagesResp: messages});

            },
            error => {
                synthesizer.close();
                reject(error);
            });
    });
};

module.exports = textToSpeech;