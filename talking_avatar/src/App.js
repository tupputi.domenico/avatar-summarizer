import React, {Suspense, useEffect, useRef, useState, useMemo} from 'react'
import {Canvas, useFrame} from '@react-three/fiber'
import {useGLTF, useTexture, Loader, Environment, useFBX, useAnimations, OrthographicCamera} from '@react-three/drei';
import {MeshStandardMaterial} from 'three/src/materials/MeshStandardMaterial';

import {LinearEncoding, sRGBEncoding} from 'three/src/constants';
import {LineBasicMaterial, MeshPhysicalMaterial, Vector2} from 'three';
import ReactAudioPlayer from 'react-audio-player';

import createAnimation from './converter';
import blinkData from './blendDataBlink.json';
import * as pdfjsLib from 'pdfjs-dist';

import * as THREE from 'three';
import axios from 'axios';

const _ = require('lodash');
let messages = [{role: "system", content: 'Lei è un utile tutor di intelligenza artificiale.'}];
pdfjsLib.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjsLib.version}/legacy/build/pdf.worker.min.js`;

const host = 'http://localhost:5000'


function Avatar({avatar_url, speak, setSpeak, setText, text, setAudioSource, playing}) {

    let gltf = useGLTF(avatar_url);
    let morphTargetDictionaryBody = null;
    let morphTargetDictionaryLowerTeeth = null;

    const [
        bodyTexture,
        eyesTexture,
        teethTexture,
        bodySpecularTexture,
        bodyRoughnessTexture,
        bodyNormalTexture,
        teethNormalTexture,
        // teethSpecularTexture,
        hairTexture,
        tshirtDiffuseTexture,
        tshirtNormalTexture,
        tshirtRoughnessTexture,
        hairAlphaTexture,
        hairNormalTexture,
        hairRoughnessTexture,
    ] = useTexture([
        "/images/body.webp",
        "/images/eyes.webp",
        "/images/teeth_diffuse.webp",
        "/images/body_specular.webp",
        "/images/body_roughness.webp",
        "/images/body_normal.webp",
        "/images/teeth_normal.webp",
        // "/images/teeth_specular.webp",
        "/images/h_color.webp",
        "/images/tshirt_diffuse.webp",
        "/images/tshirt_normal.webp",
        "/images/tshirt_roughness.webp",
        "/images/h_alpha.webp",
        "/images/h_normal.webp",
        "/images/h_roughness.webp",
    ]);

    _.each([
        bodyTexture,
        eyesTexture,
        teethTexture,
        teethNormalTexture,
        bodySpecularTexture,
        bodyRoughnessTexture,
        bodyNormalTexture,
        tshirtDiffuseTexture,
        tshirtNormalTexture,
        tshirtRoughnessTexture,
        hairAlphaTexture,
        hairNormalTexture,
        hairRoughnessTexture
    ], t => {
        t.encoding = sRGBEncoding;
        t.flipY = false;
    });

    bodyNormalTexture.encoding = LinearEncoding;
    tshirtNormalTexture.encoding = LinearEncoding;
    teethNormalTexture.encoding = LinearEncoding;
    hairNormalTexture.encoding = LinearEncoding;


    gltf.scene.traverse(node => {


        if (node.type === 'Mesh' || node.type === 'LineSegments' || node.type === 'SkinnedMesh') {

            node.castShadow = true;
            node.receiveShadow = true;
            node.frustumCulled = false;


            if (node.name.includes("Body")) {

                node.castShadow = true;
                node.receiveShadow = true;

                node.material = new MeshPhysicalMaterial();
                node.material.map = bodyTexture;
                // node.material.shininess = 60;
                node.material.roughness = 1.7;

                // node.material.specularMap = bodySpecularTexture;
                node.material.roughnessMap = bodyRoughnessTexture;
                node.material.normalMap = bodyNormalTexture;
                node.material.normalScale = new Vector2(0.6, 0.6);

                morphTargetDictionaryBody = node.morphTargetDictionary;

                node.material.envMapIntensity = 0.8;
                // node.material.visible = false;

            }

            if (node.name.includes("Eyes")) {
                node.material = new MeshStandardMaterial();
                node.material.map = eyesTexture;
                // node.material.shininess = 100;
                node.material.roughness = 0.1;
                node.material.envMapIntensity = 0.5;


            }

            if (node.name.includes("Brows")) {
                node.material = new LineBasicMaterial({color: 0x000000});
                node.material.linewidth = 1;
                node.material.opacity = 0.5;
                node.material.transparent = true;
                node.visible = false;
            }

            if (node.name.includes("Teeth")) {

                node.receiveShadow = true;
                node.castShadow = true;
                node.material = new MeshStandardMaterial();
                node.material.roughness = 0.1;
                node.material.map = teethTexture;
                node.material.normalMap = teethNormalTexture;

                node.material.envMapIntensity = 0.7;


            }

            if (node.name.includes("Hair")) {
                node.material = new MeshStandardMaterial();
                node.material.map = hairTexture;
                node.material.alphaMap = hairAlphaTexture;
                node.material.normalMap = hairNormalTexture;
                node.material.roughnessMap = hairRoughnessTexture;

                node.material.transparent = true;
                node.material.depthWrite = false;
                node.material.side = 2;
                node.material.color.setHex(0x000000);

                node.material.envMapIntensity = 0.3;


            }

            if (node.name.includes("TSHIRT")) {
                node.material = new MeshStandardMaterial();

                node.material.map = tshirtDiffuseTexture;
                node.material.roughnessMap = tshirtRoughnessTexture;
                node.material.normalMap = tshirtNormalTexture;
                node.material.color.setHex(0xffffff);

                node.material.envMapIntensity = 0.5;


            }

            if (node.name.includes("TeethLower")) {
                morphTargetDictionaryLowerTeeth = node.morphTargetDictionary;
            }

        }

    });


    const [clips, setClips] = useState([]);
    const mixer = useMemo(() => new THREE.AnimationMixer(gltf.scene), []);

    useEffect(() => {

        if (speak === false)
            return;

        makeSpeech(text, messages)
            .then(response => {

                let {blendData, filename, textResponse, messagesResp} = response.data;
                setText(textResponse)
                messages = messagesResp

                let newClips = [
                    createAnimation(blendData, morphTargetDictionaryBody, 'HG_Body'),
                    createAnimation(blendData, morphTargetDictionaryLowerTeeth, 'HG_TeethLower')];

                filename = host + filename;

                setClips(newClips);
                setAudioSource(filename);

            })
            .catch(err => {
                console.error(err);
                setSpeak(false);

            })

    }, [speak]);

    let idleFbx = useFBX('/idle.fbx');
    let {clips: idleClips} = useAnimations(idleFbx.animations);

    idleClips[0].tracks = _.filter(idleClips[0].tracks, track => {
        return track.name.includes("Head") || track.name.includes("Neck") || track.name.includes("Spine2");
    });

    idleClips[0].tracks = _.map(idleClips[0].tracks, track => {

        if (track.name.includes("Head")) {
            track.name = "head.quaternion";
        }

        if (track.name.includes("Neck")) {
            track.name = "neck.quaternion";
        }

        if (track.name.includes("Spine")) {
            track.name = "spine2.quaternion";
        }

        return track;

    });

    useEffect(() => {

        let idleClipAction = mixer.clipAction(idleClips[0]);
        idleClipAction.play();

        let blinkClip = createAnimation(blinkData, morphTargetDictionaryBody, 'HG_Body');
        let blinkAction = mixer.clipAction(blinkClip);
        blinkAction.play();


    }, []);

    // Play animation clips when available
    useEffect(() => {

        if (playing === false)
            return;

        _.each(clips, clip => {
            let clipAction = mixer.clipAction(clip);
            clipAction.setLoop(THREE.LoopOnce);
            clipAction.play();

        });

    }, [playing]);


    useFrame((state, delta) => {
        mixer.update(delta);
    });


    return (
        <group name="avatar">
            <primitive object={gltf.scene} dispose={null}/>
        </group>
    );
}


function makeSpeech(text, messages) {
    return axios.post(host + '/talk', {text: text, messages: messages});
}

const STYLES = {
    area: {position: 'absolute', display: 'flex', bottom: '10px', left: '10px', zIndex: 500},
    text: {
        margin: 'auto',
        marginBottom: "60px",
        marginLeft: "6px",
        width: '400px',
        height: '160px',
        padding: '5px',
        background: 'none',
        color: '#ffffff',
        fontSize: '1.2em',
        border: 'none'
    },
    speak: {
        margin: '6px',
        padding: '10px',
        marginRight: "auto",
        marginLeft: "10px",
        color: '#FFFFFF',
        background: '#222222',
        border: 'None'
    },
    area2: {position: 'absolute', display: 'flex', bottom: '10px', left: '10px', zIndex: 500},
    label: {color: '#777777', fontSize: '0.8em'}
}

function App() {

    const audioPlayer = useRef();

    const [speak, setSpeak] = useState(false);
    const [text, setText] = useState("Puoi riassumermi il testo in 3 punti?");
    const [audioSource, setAudioSource] = useState(null);
    const [playing, setPlaying] = useState(false);

    // End of play
    function playerEnded(e) {
        setAudioSource(null);
        setSpeak(false);
        setPlaying(false);
    }

    // Player is read
    function playerReady(e) {
        audioPlayer.current.audioEl.current.play();
        setPlaying(true);

    }

    const inputRef = useRef(null);

    const handleClick = () => {
        // 👇️ open file input box on click of another element
        inputRef.current.click();
    };

    function convert(event) {
        var fr = new FileReader();
        var pdff = new Pdf2TextClass();
        fr.onload = function () {
            pdff.pdfToText(fr.result, null, (text) => {
            });
        }
        fr.readAsDataURL(event.target.files[0])

    }

    let total_text = ''

    function Pdf2TextClass() {
        var self = this;
        this.complete = 0;

        this.pdfToText = function (data, callbackPageDone, callbackAllDone) {
            console.assert(data instanceof ArrayBuffer || typeof data == 'string');
            var loadingTask = pdfjsLib.getDocument(data);
            loadingTask.promise.then(function (pdf) {


                var total = pdf._pdfInfo.numPages;
                //callbackPageDone( 0, total );
                var layers = {};
                for (let i = 1; i <= total; i++) {
                    pdf.getPage(i).then(function (page) {
                        var n = page.pageNumber;
                        page.getTextContent().then(function (textContent) {

                            //console.log(textContent.items[0]);0
                            if (null != textContent.items) {
                                var page_text = "";
                                var last_block = null;
                                for (var k = 0; k < textContent.items.length; k++) {
                                    var block = textContent.items[k];
                                    if (last_block != null && last_block.str[last_block.str.length - 1] != ' ') {
                                        if (block.x < last_block.x)
                                            page_text += "\r\n ";
                                        else if (last_block.y != block.y && (last_block.str.match(/^(\s?[a-zA-Z])$|^(.+\s[a-zA-Z])$/) == null))
                                            page_text += ' ';
                                    }
                                    page_text += block.str + ' ';
                                    last_block = block;
                                }

                                textContent != null && console.log("page " + n + " finished."); //" content: \n" + page_text);
                                layers[n] = page_text + "\n\n";
                            }
                            ++self.complete;
                            //callbackPageDone( self.complete, total );
                            if (self.complete == total) {
                                window.setTimeout(function () {
                                    var full_text = "";
                                    var num_pages = Object.keys(layers).length;
                                    for (var j = 1; j <= num_pages; j++)
                                        full_text += layers[j];
                                    total_text = full_text;
                                    messages.push({role: "user", content: total_text})
                                }, 1000);
                            }
                        }); // end  of page.getTextContent().then
                    }); // end of page.then
                } // of for
            });
        }; // end of pdfToText()
    } // end of class


    const handleFileChange = event => {
        const fileObj = event.target.files && event.target.files[0];
        if (!fileObj) {
            return;
        }

        if (event.target.files[0].type === 'application/pdf') {
            convert(event)
        } else if (event.target.files[0].type === 'text/plain') {
            let fileReader = new FileReader();
            fileReader.onload = async (e) => {
                const text = (e.target.result)
                messages.push({role: "user", content: text})

            };
            fileReader.readAsText(event.target.files[0])
            console.log('fileObj is', fileObj);

            // 👇️ reset file input
            event.target.value = null;

            // 👇️ is now empty
            console.log(event.target.files);

            // 👇️ can still access file object here
            console.log(fileObj);
            console.log(fileObj.name);
        }
    };

    return (
        <div className="full">
            <div style={STYLES.area}>
                <textarea rows={4} type="text" style={STYLES.text} value={text}
                          onChange={(e) => setText(e.target.value.substring(0, 20000000))}/>
            </div>
            <div style={STYLES.area2}>
                <button onClick={() => setSpeak(true)} style={STYLES.speak}>{speak ? 'Running...' : 'Play'}</button>
                <button onClick={() => setText('')} style={STYLES.speak}>{'Clear'}</button>
                <input
                    style={{display: 'none'}}
                    ref={inputRef}
                    type="file"
                    onChange={handleFileChange}
                />
                <button onClick={handleClick} style={STYLES.speak}>Open file upload box</button>
            </div>

            <ReactAudioPlayer
                src={audioSource}
                ref={audioPlayer}
                onEnded={playerEnded}
                onCanPlayThrough={playerReady}

            />

            {/* <Stats /> */}
            <Canvas dpr={2} onCreated={(ctx) => {
                ctx.gl.physicallyCorrectLights = true;
            }}>

                <OrthographicCamera
                    makeDefault
                    zoom={1100}
                    position={[0, 1.56, 1]}
                />

                {/* <OrbitControls
        target={[0, 1.65, 0]}
      /> */}

                <Suspense fallback={null}>
                    <Environment background={false} files="/images/photo_studio_loft_hall_1k.hdr"/>
                </Suspense>

                <Suspense fallback={null}>
                    <Bg/>
                </Suspense>

                <Suspense fallback={null}>


                    <Avatar
                        avatar_url="/model.glb"
                        speak={speak}
                        setSpeak={setSpeak}
                        setText={setText}
                        text={text}
                        setAudioSource={setAudioSource}
                        playing={playing}
                    />


                </Suspense>


            </Canvas>
            <Loader dataInterpolation={(p) => `Loading... please wait`}/>
        </div>
    )
}

function Bg() {

    const texture = useTexture('/images/bg.webp');

    return (
        <mesh position={[0, 1.5, -2]} scale={[0.8, 0.8, 0.8]}>
            <planeBufferGeometry/>
            <meshBasicMaterial map={texture}/>

        </mesh>
    )

}

export default App;
